# CoCalc with cuda in a container🐳 for use with PyTorch🐍
This image can be used to run [CoCalc](https://cocalc.com/) locally without the harware limitations of the official free tier. Specifically, your graphics card can be utilized. CoCalc is similar to [Jupyter](https://jupyter.org/) but features real-time collaboration as opposed to for example Google Colab.

The image is based on the official [CoCalc Docker image](https://github.com/sagemathinc/cocalc-docker).

## Preface
Only one of your team members needs to run this. Preferably the one with the beefiest rig (Nvidia GPU). I don't think a Laptop can take this tbh. The image is ~12GB in download and ~32GB in size on disk.

There are other attempts [here](https://github.com/logchan/cocalc-torch/blob/master/Dockerfile) and [here](https://github.com/ktaletsk/CuCalc/blob/master/Dockerfile), just not as up-to-date.

The steps are described with linux in mind but should transfer to any other OS.

I am far from an expert on the Nvidia container toolkit and cannot promise anything working.

The setup is still not straight forward and takes some steps. If you don't have some time on your hands and some knowledge about your computer you're probably better off just sharing your screen or using Colab. Especially some container knowledge is recommended.

I am just a student, so I cannot provide extensive support.

Sharing the container on the public internet is a small security risk depending how you do it (see below).

## How to use this
### Prerequisites
- Have Docker installed on your host. The instructions are found [here](https://docs.docker.com/get-docker/).
- Have the [Nvidia container **toolkit**](https://github.com/NVIDIA/nvidia-docker) installed on your system. Instructions can be found [here](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker). I am on archlinux, [this](https://wiki.archlinux.org/index.php/Docker#With_NVIDIA_Container_Toolkit_(recommended)) worked for me. Test your setup by running the following command:
	```
	docker run --rm --gpus all nvidia/cuda nvidia-smi
	```

### First steps
1. Once docker is running, you should be able to execute the `./start.sh` script in this repository or the one-liner inside. You do not need to clone the repository or build the image yourself.
2. When the download is finished, after a few minutes you should be able to visit https://localhost in your browser. The image will not need to be downloaded the next time.
3. Register an account. You can use an imaginary email address. The account is only stored on your computer.
4. Optional, recommended if using a tunnel like ngrok (see below):
	1. [Make yourself an admin](https://github.com/sagemathinc/cocalc-docker/#make-a-user-an-admin)
	2. [Restrict who can make accounts on your server](https://github.com/sagemathinc/cocalc-docker/#make-a-user-an-admin)
5. Make your CoCalc instance accessible to your collaborators. There are countless methods to achieve this. The safest is probably to establish a VPN between your collaborators. I haven't done this in a long time so I don't know what software to use. The easiest is probably to use a tunnel like [ngrok](https://ngrok.com/). To use ngrok, [download](https://ngrok.com/download) it and execute `./ngrok http 443`. Ngrok will give you a public link (https) to share with your collaborators.
6. Let you collaborators register accounts (on your instance only), create a Project and add them. Your instance cannot send emails, but you can add collaborators anyways.
7. When uploading your `*.ipynb` files to your cocalc instance, select the "Conda's Python for Pytorch with Cuda" kernel.

### Shutdown
When you want to stop cocalc terminate, if applicable, your ngrok session and run the following command:
```
docker stop cocalc
```
Your projects, accounts, and other data will be saved for when you start cocalc again.

## Security
Exposing a server to the public internet (ngrok) is always a small security risk. Since docker is used here, the risk is reduced significantly because even if someone would exploit you cocalc instance and somehow gain root access to it, that access would be limited to the container and cannot do damage to your host unless the attacker knows a docker exploit at the same time. See also the [security of the official cocalc image](https://doc.cocalc.com/docker-image.html#security-status) and the [security of docker](https://docs.docker.com/engine/security/) if you are interested.

Hints:
- Never run this image with the `--priviledged` flag or mount (`-v`) volumes if you don't understand the implications. Just don't change the `./start.sh` script and you're good 😅.
- Using a VPN between you and your collaborators reduces the threat to your collaborators only.
- Following step 4. above prevents unwanted people from creating accounts when using a public tunnel like ngrok, but nobody should know your ngrok URL anyways so it is not strictly necessary.


## Contributing
Merge Requests / Issues / Feedback are greatly appreciated.