#!/bin/bash
docker run \
	--rm \
	-d \
	--shm-size=3g \
	--gpus=all \
	--name=cocalc \
	-v cocalc:/projects \
	-p 443:443 \
	docker.gitlab.gwdg.de/j.hoerdt/cocalc-cuda-pytorch