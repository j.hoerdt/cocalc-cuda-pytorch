FROM sagemathinc/cocalc

ENV NVIDIA_DRIVER_CAPABILITIES="compute,utility" NVIDIA_VISIBLE_DEVICES="all"

#full upgrade
RUN apt update && apt upgrade -y

#install cuda
RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin  \
 && mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600 \
 && apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub \
 && add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /" \
 && apt-get update \
 && apt-get -y install cuda

# install miniconda
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -q -O ~/miniconda.sh \
 && bash ~/miniconda.sh -b -p $HOME/miniconda \
 && /root/miniconda/bin/conda init \
 && chmod go+rx /root

# install critical packages
RUN /root/miniconda/bin/conda install pytorch torchvision torchaudio cudatoolkit=11.0 ipykernel -c pytorch
# install additional packages
RUN /root/miniconda/bin/conda install scikit-learn seaborn fastprogress ipywidgets -c conda-forge

# create custom kernel
COPY kernel.json /usr/local/share/jupyter/kernels/python-conda-cuda-pytorch/